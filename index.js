var AWS = require('aws-sdk');
var DOC = require('dynamodb-doc');
var dynamo = new DOC.DynamoDB();

var https = require('https');

var networks = ["Instagram"];

//Callback functions
var error = function (err, response, body) {
    console.log('ERROR [%s]', err);
};
var success = function (data) {
    console.log('Data [%s]', data.toString());
};

var cb = function(err, data) {
    if(err) {
        console.log(err);
    } else {

    }
};

var messageOutput = "Results\n";

exports.handler = function(event, context) {
    var cb = function(err, data) {
        if(err) {
            console.log('error on RetrieveSocialFeed: ',err);
            context.done('Unable to retrieve Social Site Data', null);
        } else {
            data.Items.forEach(function(item) {
                if (item && item.SiteId) {
                    networks.forEach(function (elem) {
                        if (item[[elem] + "Status"]) {
                            loopThroughNetworks(elem, item[[elem] + "Id"], item.SiteId);
                        }
                    });
                }
            });
        }
    };

    var params = {
        TableName : "SocialSiteSettings",
        FilterExpression: "SiteStatus = :siteStatus",
        ExpressionAttributeValues: {
            ":siteStatus": true,
        }
    };

    dynamo.scan(params, cb);
};

function loopThroughNetworks(network, networkId, siteId) {
    // call the correct function for the network in question returning a count of items found
    eval([network] + "Feed")(networkId, siteId);
}

function InstagramFeed(networkId, siteId) {

    var options = {
      host: 'www.instagram.com',
      path: "/" + networkId + "/media/",
      port: 443
    };

    https.get(options, function(res){
        var body = '';
    
        res.on('data', function(chunk){
            body += chunk;
        });
    
        res.on('end', function(){
            var fbResponse = JSON.parse(body);
            for(var attributename in fbResponse.items){
            
                var item = {
                        ItemId: siteId + fbResponse.items[attributename].created_time,
                        SiteId: Number(siteId),
                        FeedType: "Instagram",
                        DateCreated: Number(fbResponse.items[attributename].created_time),
                        Title: fbResponse.items[attributename].images.standard_resolution.url,
                        Active: true
                    };
                dynamo.putItem({TableName:"SocialFeedsData", Item:item}, cb);
            }
        });
    }).on('error', function(e){
          console.log("Got an error: ", e);
    });

}
